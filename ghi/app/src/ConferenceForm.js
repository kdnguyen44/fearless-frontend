import React, { useEffect, useState } from 'react';

function ConferenceForm(props) {
    const [locations, setLocations] = useState([]);
    const [name, setName] = useState('');
    const [startDate, setStartDate] = useState('');
    const [endDate, setEndDate] = useState('');
    const [description, setDescription] = useState('');
    const [maxPresentations, setMaxPresentations] = useState('');
    const [maxAttendees, setMaxAttendees] = useState('');
    const [location, setLocation] = useState('');

    const handleNameChange = (e) => {
        const value = e.target.value;
        setName(value);
    }

    const handleStartDateChange = (e) => {
        const value = e.target.value;
        setStartDate(value);
    }

    const handleEndDateChange = (e) => {
        const value = e.target.value;
        setEndDate(value);
    }

    const handleDescriptionChange = (e) => {
        const value = e.target.value;
        setDescription(value);
    }

    const handleMaxPresentationChange = (e) => {
        const value = e.target.value;
        setMaxPresentations(value);
    }

    const handleMaxAttendeesChange = (e) => {
        const value = e.target.value;
        setMaxAttendees(value);
    }

    const handleLocationChange = (e) => {
        const value = e.target.value;
        setLocation(value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {};

        data.name = name;
        data.starts = startDate;
        data.ends = endDate;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;

        console.log(data);

        const locationURL = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        };
        const response = await fetch(locationURL, fetchConfig);
        if(response.ok) {
            const newConference = await response.json();
            console.log(newConference)
            setName('');
            setStartDate('');
            setEndDate('');
            setDescription('');
            setMaxPresentations('');
            setMaxAttendees('');
            setLocation('');
        }

    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={startDate} onChange={handleStartDateChange} placeholder="Start Date" required type="date" name="starts" id="starts" className="form-control"/>
                <label htmlFor="start">Start date:</label>
              </div>
              <div className="form-floating mb-3">
                <input value={endDate} onChange={handleEndDateChange} placeholder="End Date" required type="date" name="ends" id="ends" className="form-control"/>
                <label htmlFor="end">End date:</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description">Description</label>
                <textarea value={description} onChange={handleDescriptionChange} name="description" id="description" className="form-control" rows="3"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input value={maxPresentations} onChange={handleMaxPresentationChange} placeholder="Max presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                <label htmlFor="max_pres">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input value={maxAttendees} onChange={handleMaxAttendeesChange} placeholder="Max attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                <label htmlFor="max_att">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select value={location} onChange={handleLocationChange} required id="location" name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return (
                        <option key={location.id} value={location.id}>
                            {location.name}
                        </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default ConferenceForm;

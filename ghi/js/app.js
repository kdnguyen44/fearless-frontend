function createCard(name, description, pictureUrl, startDate, endDate, locationName) {
    return `
        <div class="col-3">
            <div class="card shadow mb-4 bg-body-tertiary rounded">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-body-secondary">${locationName}</h6>
                <p class="card-text">${description}</p>
                </div>
                <div class="card-footer"><p>${startDate} - ${endDate}</p></div>
            </div>
        </div>
    `;
}

function formatDate(date){
    const newDate = new Date(date);
    const month = newDate.getMonth()
    const day = newDate.getDate();
    const year = newDate.getFullYear();

    return month + "/" + day + "/" + year;
}

// code from bootsrap

// var alertPlaceholder = document.getElementById('liveAlertPlaceholder')
// var alertTrigger = document.getElementById('liveAlertBtn')

// function alert(message, type) {
//   var wrapper = document.createElement('div')
//   wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'

//   alertPlaceholder.append(wrapper)
// }

// if (alertTrigger) {
//   alertTrigger.addEventListener('click', function () {
//     alert('Nice, you triggered this alert message!', 'success')
//   })
// }


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            throw new Error('Response not ok')
            // Figure out what to do when the response is bad
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const startDateObject = details.conference.starts;
                    const endDateObject = details.conference.ends;
                    const startDate = formatDate(startDateObject);
                    const endDate = formatDate(endDateObject)
                    const locationName = details.conference.location.name;
                    const html = createCard(title, description, pictureUrl, startDate, endDate, locationName);
                    const column = document.querySelector('.row');
                    column.innerHTML += html;
                }
            }
        }
    } catch (error) {
        // Figure out what to do if an error is raised
        console.error('error', error);
    };


});
